#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
 

int main(){

	int origen,destino;
	char c;
	origen = open("origen.txt",O_RDONLY);

	destino = open("destino.txt",O_WRONLY|O_CREAT,S_IRUSR|S_IWUSR);
 
	if(origen!=-1){
		//El archivo se lee caracter por caracter
		while(read(origen,&c,sizeof(c)!=0)){
		//guardar
		write(destino,&c,sizeof(c));
	}
 
	//cerrar el archivo de origen y destino
	close(origen);
	close(destino);
 
	}
}
